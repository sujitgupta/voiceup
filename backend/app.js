const http = require('http');

const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({extended:false}));

app.get('/',(req,res,next)=>{
 res.send({response:{board:[
     {
         id:006,
         title:'atmoshpheric Window',
         text:'harvest the power of Zeus fo litting your lamps!!',
         postedBy:'userId',
         echo:['userId01','userId02','userId03','userId04'],
         mute:['userId07','userId06','userId05','userId07'],
         mood:'eureka',
         tags:{
             skills:['Engineering','Science','Inventions','Electricity','Energy'],
             interests:['Energy','Resources','Engineering','SocioEconomic']
         },
        rectification:"",
        rectified:false,
        socialWorld:{
            twitter:{
                retweetd:91,
                shares:23,
                reach:400
            }
        }
     },
     {
        id:007,
        title:'Mysterious Virus',
        text:'harmful for livestocks and can spread in human from there, can cause high fever!!',
        postedBy:'userId',
        echo:['userId01','userId02','userId03','userId04'],
        mute:['userId07','userId06','userId05','userId07'],
        mood:'alarm',
        tags:{
            skills:['BioTech','MicroOrganism','Virology'],
            interests:['Social','Enviroment','HealthCare','Pandemics','Research','Science']
        },
        rectification:"",
        rectified:false,
        socialWorld:{
            twitter:{
                retweetd:91,
                shares:23,
                reach:400
            }
        }
    }
 ]}});
});
app.get('/login',(req,res,next)=>{
    res.send({response:{login:true}});
});
app.get('/signup',(req,res,next)=>{
    res.send({login:true});
});
app.post('/boards',(req,res,next)=>{
    res.send({
        title:'Mysterious Virus',
        text:'harmful for livestocks and can spread in human from there, can cause high fever!!',
        postedBy:'userId',
        createdAt:'time',
        mood:'alarm',
        tags:{
            skills:['BioTech','MicroOrganism','Virology'],
            interests:['Social','Enviroment','HealthCare','Pandemics','Research','Science']
        }
    });
});

const server = http.createServer(app);

server.listen(3000);
